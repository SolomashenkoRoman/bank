## Clone the project:
```
#!bash

git clone https://SolomashenkoRoman@bitbucket.org/SolomashenkoRoman/bank.git

```
## Cd into project directory:
```
#!bash

cd bank
```
## Install dependencies:
```
#!bash

composer install
npm install
```
## Create a database:
```
#!bash

php bin/console doctrine:database:create
```

## Generate database migrations:
```
#!bash

php bin/console doctrine:migrations:generate
php bin/console doctrine:migrations:diff
```
## Migrate the database:
```
#!bash

php bin/console doctrine:migrations:migrate
```
## Load dummy data:
```
#!bash

php bin/console doctrine:fixtures:load
```
## Encore scripts:
```
#!bash

npm run dev
```

## Start a server:
```
#!bash

php bin/console server:start
```


Go to http://127.0.0.1:8000 and login with:
login: admin
paswd: admin

At the bottom of the page you can see API token to access API endpoints (pass it as X-AUTH-TOKEN header).

## Route
```
#!bash

php bin/console debug:router
```

## Command to calculate the sum of all transactions from previous day (it stores the sum to summary table):

```
#!bash

php bin/console app:summary
```

## Set up the cron job to run every 2 days at 23:47:
```
#!bash

crontab -e
```

## Create the cron command:
```
#!bash

47 23 */2 * * php [path_to_the_project_directory]/bin/console app:summary > /dev/null