<?php
/**
 * Created by PhpStorm.
 * User: romansolomashenko
 * Date: 7/6/18
 * Time: 11:55
 */

namespace App\Repository;


use App\Entity\Transaction;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Psr\Log\LoggerInterface;

class TransactionRepository extends ServiceEntityRepository
{
    public $logger;
    public function __construct(RegistryInterface $registry, LoggerInterface $logger)
    {
        parent::__construct($registry, Transaction::class);
        $this->logger = $logger;
    }

    public function findByFilter($customerId, $amount, $date, $offset = 0, $limit = 10)
    {
        $offset = is_null($offset) ? 0 : $offset;
        $limit = is_null($limit) ? 10 : $limit;
        $queryBuilder = $this->createQueryBuilder('t');
        if ( ! is_null($customerId)) {
            $queryBuilder->andWhere('t.customer = :customer')
                ->setParameter('customer', $customerId);
        }
        if ( ! is_null($amount)) {
            $queryBuilder->andWhere('t.amount = :amount')
                ->setParameter('amount', $amount);
        }
        if ( ! is_null($date)) {
            $queryBuilder->andWhere('t.date = :date')
                ->setParameter('date', $date);
        }
        return $queryBuilder->orderBy('t.date', 'DESC')
            ->setFirstResult($offset)
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult()
            ;
    }

    public function summary($date = null)
    {
        if ( ! $date) {
            $date = new \DateTime();
        }
        $this->logger->debug("summary", [$date->format('Y-m-d')]);
        return $this->createQueryBuilder('t')
            ->andWhere('t.date = :date')
            ->setParameter('date', $date->format('Y-m-d'))
            ->select('SUM(t.amount) as summary')
            ->getQuery()
            ->getOneOrNullResult();
    }
}