<?php
/**
 * Created by PhpStorm.
 * User: romansolomashenko
 * Date: 7/8/18
 * Time: 21:12
 */

namespace App\Controller\Api;


use App\Entity\Customer;
use App\Entity\Transaction;
use App\Repository\TransactionRepository;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class TransactionController extends AbstractController
{
    /**
     * @Route("api/transaction")
     * @Method("GET")
     */
    public function index(Request $request, TransactionRepository $transactions)
    {
        $foundTransactions = $transactions->findByFilter(
            $request->query->get('customerId'),
            $request->query->get('amount'),
            $request->query->get('date'),
            $request->query->get('offset'),
            $request->query->get('limit')
        );
        $results = [];
        foreach ($foundTransactions as $transaction) {
            $results[] = [
                'transactionId' => $transaction->getId(),
                'customerId' => $transaction->getCustomer()->getId(),
                'amount' => $transaction->getAmount(),
                'date' => $transaction->getDate()->format('d.m.Y')
            ];
        }
        return new JsonResponse([
            'status' => 'success',
            'data' => $results]);
    }

    /**
     * @Route("api/transaction/{customer}/{transaction}")
     * @Method("GET")
     */
    public function read(Customer $customer, Transaction $transaction)
    {
        return new JsonResponse([
            'status' => 'success',
            'transactionId' => $transaction->getId(),
            'amount' => $transaction->getAmount(),
            'date' => $transaction->getDate()->format('d.m.Y')]);

    }

    /**
     * @Route("api/transaction")
     * @Method("POST")
     */
    public function create(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $customerRepository = $em->getRepository(Customer::class);
        $customerId = $request->request->get('customerId');
        $customer = $customerRepository->find($customerId);
        if ( ! $customer) {
            return new JsonResponse([
                    'status' => 'fail',
                    'msg' => 'No customer found for id ' . $customerId]);
        }
        $transaction = new Transaction();
        $transaction->setCustomer($customer);
        $transaction->setAmount($request->request->get('amount'));
        $em->persist($transaction);
        $em->flush();
        return new JsonResponse([
                'status' => 'success',
                'transactionId' => $transaction->getId(),
                'customerId' => $customer->getId(),
                'amount' => $transaction->getAmount(),
                'date' => $transaction->getDate()->format('d.m.Y')]);
    }

    /**
     * @Route("api/transaction/{transaction}")
     * @Method({"PUT", "PATCH"})
     */
    public function update(Request $request, Transaction $transaction)
    {
        $em = $this->getDoctrine()->getManager();
        $transaction->setAmount($request->query->get('amount'));
        $em->persist($transaction);
        $em->flush();
        return new JsonResponse([
            'status' => 'success',
            'transactionId' => $transaction->getId(),
            'customerId' => $transaction->getCustomer()->getId(),
            'amount' => $transaction->getAmount(),
            'date' => $transaction->getDate()->format('d.m.Y')]);
    }

    /**
     * @Route("api/transaction/{transaction}")
     * @Method("DELETE")
     */
    public function delete(Transaction $transaction)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($transaction);
        $em->flush();
        return new JsonResponse([
            'status' => 'success']);
    }
}