<?php
/**
 * Created by PhpStorm.
 * User: romansolomashenko
 * Date: 7/6/18
 * Time: 11:59
 */

namespace App\Controller\Api;

use App\Entity\Customer;
use App\Repository\CustomerRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class CustomerController extends AbstractController
{
    /**
     * @Route("api/customer")
     * @Method("GET")
     */
    public function index(Request $request, CustomerRepository $customers)
    {
        $foundCustomers = $customers->findByFilter(
            $request->query->get('name'),
            $request->query->get('offset'),
            $request->query->get('limit')
        );
        $results = [];
        foreach ($foundCustomers as $customer) {
            $results[] = [
                'customerId' => $customer->getId(),
                'name' => $customer->getName(),
                'cpn' => $customer->getCnp(),
            ];
        }
        return new JsonResponse([
            'status' => 'success',
            'data' => $results]);
    }

    /**
     * @Route("api/customer")
     * @Method("POST")
     */
    public function create(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $customer = new Customer();
        $customer->setName($request->request->get('name'));
        $customer->setCnp($request->request->get('cnp'));
        $em->persist($customer);
        $em->flush();
        return new JsonResponse([
            'status' => 'success',
            'customerId' => $customer->getId()]
        );
    }
}