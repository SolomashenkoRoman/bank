<?php
/**
 * Created by PhpStorm.
 * User: romansolomashenko
 * Date: 7/9/18
 * Time: 03:53
 */

namespace App\DataFixture;

use App\Entity\Customer;
use App\Entity\Transaction;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;

class AppFixtures extends Fixture
{
    private $encoder;
    private $tokenGenerator;

    public function __construct(UserPasswordEncoderInterface $encoder, TokenGeneratorInterface $tokenGenerator)
    {
        $this->encoder = $encoder;
        $this->tokenGenerator = $tokenGenerator;
    }

    public function load(ObjectManager $manager)
    {
        $this->loadUsers($manager);
        $this->loadCustomers($manager);
        $this->loadTransactions($manager);
    }

    private function loadUsers(ObjectManager $manager)
    {
        foreach ($this->getUserData() as $value) {
            $user = new User();
            $user->setUsername($value['username']);
            $user->setEmail($value['email']);
            $user->setPassword($this->encoder->encodePassword($user, $value['password']));
            $user->setToken($this->tokenGenerator->generateToken());
            $manager->persist($user);
        }
        $manager->flush();
    }

    private function loadCustomers(ObjectManager $manager)
    {
        foreach ($this->getCustomerData() as $key => $value)  {
            $customer = new Customer();
            $customer->setName($value['name']);
            $customer->setCnp($value['cnp']);
            $manager->persist($customer);
            $this->addReference('customer-' . $key, $customer);
        }
        $manager->flush();
    }

    private function loadTransactions(ObjectManager $manager)
    {
        $maxCustomer = count($this->getCustomerData()) - 1;
        for ($i = 100; $i--;) {
            $transaction = new Transaction();
            $customer = $this->getReference('customer-' . random_int(0, $maxCustomer));
            $transaction->setCustomer($customer);
            $transaction->setAmount(random_int($i, 500));
            $transaction->setDate(new \DateTime('now - ' . random_int(0, 30) . ' days'));
            $manager->persist($transaction);
        }

        $manager->flush();
    }

    private function getUserData(){
        return [
            ['username' => 'admin', 'password' => 'admin', 'email' => 'admin@gmai.com'],
            ['username' => 'user', 'password' => 'qwerty', 'email' => 'user@gmai.com']
        ];
    }

    private function getCustomerData()
    {
        return [
            ['name' => 'Admin Admin', 'cnp' => '1234567890'],
            ['name' => 'User User', 'cnp' => '1234567891'],
            ['name' => 'Richard Young', 'cnp' => '1234567892'],
            ['name' => 'Joshua Young', 'cnp' => '1234567893']
        ];
    }
}