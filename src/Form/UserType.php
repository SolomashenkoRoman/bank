<?php
/**
 * Created by PhpStorm.
 * User: romansolomashenko
 * Date: 7/4/18
 * Time: 18:54
 */

namespace App\Form;


use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', TextType::class, array(
                'label' => 'Username',
                'label_attr' => array(
                    'class' => 'sr-only'
                ),
                'attr' => array(
                    'placeholder' => 'Username',
                    'class' => 'form-control',
                    'autofocus' => ''
                ),
                'required' => true
            ))
            ->add('email', EmailType::class, array(
                'label' => 'Email',
                'label_attr' => array(
                    'class' => 'sr-only'
                ),
                'attr' => array(
                    'placeholder' => 'Email',
                    'class' => 'form-control'
                ),
                'required' => true
            ))
            ->add('plainPassword', RepeatedType::class, array(
                'type' => PasswordType::class,
                'first_options' => array(
                    'label' => 'Password',
                    'label_attr' => array(
                        'class' => 'sr-only'
                    ),
                    'attr' => array(
                        'placeholder' => 'Password',
                        'class' => 'form-control',
                        'autofocus' => ''
                    ),
                    'required' => true
                ),
                'second_options' => array(
                    'label' => 'Repeat Password',
                    'label_attr' => array(
                        'class' => 'sr-only'
                    ),
                    'attr' => array(
                        'placeholder' => 'Repeat Password',
                        'class' => 'form-control form-input-repeat-password',
                        'autofocus' => ''
                    ),
                    'required' => true
                ),
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => User::class,
        ));
    }
}