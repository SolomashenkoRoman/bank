require('jquery');
require('bootstrap');
require('bootstrap-datepicker');
$(function() {

    let page = {
        userToken: '',
        init: function () {
            this.userToken = $('body').data('user-token');
            console.log($('body'));
            console.log(this.userToken);
            if ($('body').hasClass("dashboard")) {
                this.initPageDashboard();
            } else if ($('body').hasClass("customer")) {
                this.initPageCustomer();
            }
        },
        initPageDashboard: function () {
            let self = this;
            this.getTransaction('');
            $( '#transactionDate' ).datepicker({format: 'yyyy-mm-dd'});
            $( '#transactionCustomerId' ).keyup(self.fiterTransaction);
            $( '#transactionAmount' ).keyup(self.fiterTransaction);
            $( '#transactionDate' ).change(self.fiterTransaction);
        },
        fiterTransaction: function(){
            let customerId = $('#transactionCustomerId').val();
            let amount = $('#transactionAmount').val();
            let date = $('#transactionDate').val();
            if (customerId != ''){
                customerId = 'customerId='+customerId;
            }
            if (amount != ''){
                amount = 'amount='+amount;
            }
            if (date != ''){
                date = 'date='+date;
            }
            console.log('fiterTransaction');
            console.log(customerId);
            console.log(amount);
            console.log(date);
            let url = '';
            url = page.addQueryString(url, customerId)
            url = page.addQueryString(url, amount)
            url = page.addQueryString(url, date)
            console.log(url);
            page.getTransaction(url);
        },
        addQueryString: function(url, queryString){
            let isQuestionMarkPresent = url && url.indexOf('?') !== -1,
                separator = '';
            if (queryString) {
                separator = isQuestionMarkPresent ? '&' : '?';
                url += separator + queryString;
            }

            return url;
        },
        getTransaction: function(queryString){
            let self = this;
            let url = '/api/transaction'+queryString;
            let xhr = new XMLHttpRequest();
            xhr.open("GET", url, true);
            xhr.setRequestHeader('X-AUTH-TOKEN', this.userToken);
            xhr.onreadystatechange = function() {
                if (xhr.readyState == XMLHttpRequest.DONE) {
                    // JSON.parse does not evaluate the attacker's scripts.
                    let response = JSON.parse(xhr.response);
                    if (response.status == 'success'){
                        console.log('success');
                        self.viewTransaction(response.data);
                    }
                    console.log(response);
                }
            }
            xhr.send();
        },
        viewTransaction: function(transactions){
            console.log(transactions);
            $('tbody').empty();
            let tbody = '';
            $.each(transactions, function(i, transaction) {
                console.log(transaction);
                tbody += '<tr>'
                    +'<td>'+transaction.transactionId+'</td>'
                    +'<td>'+transaction.customerId+'</td>'
                    +'<td>'+transaction.amount+'</td>'
                    +'<td>'+transaction.date+'</td>'
                    + '</tr>'
            });
            $('tbody').append(tbody);
        },
        initPageCustomer: function () {
            let self = this;
            this.getCustomer(name);
            $( '#customerName' ).keyup(function() {
                let name = $(this).val();
                console.log(name);
                self.getCustomer(name)
            });
        },
        getCustomer: function(name){
            let self = this;
            let url = '/api/customer';
            if (name !== ''){
                url += '?name='+name;
            }
            let xhr = new XMLHttpRequest();
            xhr.open("GET", url, true);
            xhr.setRequestHeader('X-AUTH-TOKEN', this.userToken);
            xhr.onreadystatechange = function() {
                if (xhr.readyState == XMLHttpRequest.DONE) {
                    // JSON.parse does not evaluate the attacker's scripts.
                    let response = JSON.parse(xhr.response);
                    if (response.status == 'success'){
                        console.log('success');
                        self.viewCustomer(response.data);
                    }
                    console.log(response);
                    console.log(url);
                }
            }
            xhr.send();
        },
        viewCustomer: function(customers){
            console.log(customers);
            $('tbody').empty();
            let tbody = '';
            $.each(customers, function(i, customer) {
                console.log(customer);
                tbody += '<tr>'
                    +'<td>'+customer.customerId+'</td>'
                    +'<td>'+customer.name+'</td>'
                    +'<td>'+customer.cpn+'</td>'
                    + '</tr>'
            });
            $('tbody').append(tbody);
        },

    }
    page.init();
});

